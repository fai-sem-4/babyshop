<%-- 
    Document   : result
    Created on : Jun 18, 2019, 3:01:27 PM
    Author     : coivn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Product List Of Search <s:property value="keyword"/></title>
    </head>
    <body>
        <h1>Product List Of Search <s:property value="keyword"/></h1>
        <table>
            <s:iterator value="product" var="product">
                <tr>
                    <td><s:property value="name"/></td>
                    <td><s:property value="price"/></td>
                    <td><s:property value="description"/></td>
                    <td><a href="addToCart?newProductId=<s:property value="id"/>">Add To Cart<a/></td>
                </tr>
            </s:iterator>
                <a href="index.isp">Back</a>
        </table>
    </body>
</html>
