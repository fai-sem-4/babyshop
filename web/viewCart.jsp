<%-- 
    Document   : viewCart
    Created on : Jun 18, 2019, 3:01:44 PM
    Author     : coivn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cart Detail</title>
    </head>
    <body>
        <h1>Cart Detail</h1>
        <table border="1">
            <tr>
                <th>Product</th>
                <th>Amount</th>
            </tr>
            <s:iterator value="product" var="product">
                <tr>
                    <td><s:property value="key.name"/></td>
                    <td><s:property value="value"/></td>
                </tr>
            </s:iterator>
                
        </table>
        <label>Total: </label><s:property value="total"/><br>
        <a href="index.isp">Continue Buying</a>
    </body>
</html>
