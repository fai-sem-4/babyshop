/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baby.shop.da;

import baby.shop.entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author coivn
 */
public class ProductManager {
    private static PreparedStatement searchByNameStatement;
    private static PreparedStatement searchByIdStatement;
    private PreparedStatement getSearchByNameStatement() throws ClassNotFoundException, SQLException{
        if (searchByNameStatement == null) {
            // add connect
            Connection connection = DBConnection.getConnection();
            
            searchByNameStatement = connection.prepareStatement("select id, name,price, description from product like name = ?");
        }
        return searchByNameStatement;
    }
    
    private PreparedStatement getSearchByIdStatement() throws ClassNotFoundException, SQLException{
        if (searchByIdStatement == null) {
            // add connect
            Connection connection = DBConnection.getConnection();
            
            searchByIdStatement = connection.prepareStatement("select id, name,price, description from product like id = ?");
        }
        return searchByIdStatement;
    }
    
    public List<Product> getProductsByName(String keyword) throws SQLException, ClassNotFoundException{
        try{
            PreparedStatement statement = getSearchByNameStatement();
            statement.setString(1, "%" + keyword + "%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int price = rs.getInt("price");
                String description = rs.getString("description");
                products.add(new Product(id,name,price,description));
            
            }
            return products;
        } catch(Exception ex){
            
          //  Logger.getLogger(baby.shop.da.ProductManager.class.getName()).log(Level.SEVERE,null,ex);
            return new LinkedList<Product>();
        }
        
    }
    
    public Product getProductById(int id){
        try {
             PreparedStatement statement = getSearchByIdStatement();
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                String name = rs.getString("name");
                int price = rs.getInt("price");
                String description = rs.getString("description");
               return new Product(id,name,price,description);
            
            }
         
            
        } catch (Exception e) {
           // Logger.getLogger(baby.shop.da.ProductManager.class.getName()).log(Level.SEVERE,null,e);
        }
        return new Product (0,"",0,"");
    }
}
