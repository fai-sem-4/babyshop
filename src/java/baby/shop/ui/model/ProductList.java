/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baby.shop.ui.model;

import baby.shop.da.ProductManager;
import baby.shop.entity.Product;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;

/**
 *
 * @author coivn
 */
public class ProductList extends ActionSupport {
    private String keyword;
    private List<Product> product;
    public ProductList() {
    }
    
    public String execute() throws Exception {
        product = new ProductManager().getProductsByName(keyword);
        return SUCCESS;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    
    public String getKeyword(){
        return keyword;
    }
    
    public List<Product> getProducts(){
        return product;
    }
    
}

    
    