/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baby.shop.ui.model;

import baby.shop.biz.Cart;
import baby.shop.entity.Product;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;

/**
 *
 * @author coivn
 */
public class ViewCard extends ActionSupport {
    private Map<Product, Integer> product;
    private float total;

    @Override
    public String execute() throws Exception {
        Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
        if(cart == null){
            return ERROR;
        }
        product = cart.getProduct();
        total = cart.getTotalPrice();
        return SUCCESS;
//To change body of generated methods, choose Tools | Templates.
    }
    public Map<Product, Integer> getProducts(){
        return product;
    }
    public float getTotal(){
        return total;
    }
    
}
